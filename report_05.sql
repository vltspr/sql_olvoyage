SELECT t.train_id "Train", d.city || TO_CHAR(t.departure_time, '(DD/MM/YY HH24:MI)') ||' - '|| a.city || TO_CHAR(t.arrival_time, '(DD/MM/YY HH24:MI)') " Trajet ", t.distance " Distance", t.price ||'�' " Prix initial "
FROM T_TRAIN t
JOIN T_STATION d
ON t.departure_station_id = d.station_id
JOIN T_STATION a
ON t.arrival_station_id = a.station_id 
ORDER BY "Train"
/
