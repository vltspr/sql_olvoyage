SELECT tr.train_id, stDep.city ||' - '|| stArr.city, tr.distance/((tr.arrival_time-tr.departure_time)*24)||'km/h' vitesse
FROM t_train tr
JOIN t_station stDep
ON tr.departure_station_id = stDep.station_id    
JOIN t_station stArr
ON tr.arrival_station_id = stArr.station_id
ORDER BY vitesse;