SELECT DISTINCT(CONCAT(UPPER(c.last_name), CONCAT(' ', c.first_name))) "Acheteur"
FROM T_RESERVATION r
JOIN T_TICKET t
ON t.customer_id <> r.buyer_id
JOIN T_CUSTOMER c
ON c.customer_id = r.buyer_id
ORDER BY "Acheteur";
