SELECT * 
FROM (
	SELECT CONCAT(UPPER(e.last_name), CONCAT(' ',e.first_name)) "NOM Pr�nom"
	FROM t_employee e 
	JOIN t_reservation r
	ON e.employee_id = r.employee_id
	GROUP BY e.employee_id, e.last_name, e.first_name
	ORDER BY COUNT(r.reservation_id) DESC
	)
WHERE ROWNUM <=1;
