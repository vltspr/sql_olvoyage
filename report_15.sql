SELECT LPAD(UPPER(emp.last_name) || ' ' || emp.first_name || ' ' || NVL(reserv.countreserv, 0), LENGTH(UPPER(emp.last_name) || ' ' || emp.first_name || ' ' || NVL(reserv.countreserv, 0)) + (LEVEL*2), '+') "Employee"
FROM T_EMPLOYEE emp
FULL OUTER JOIN
  (SELECT employee_id,
          COUNT(reservation_id) countreserv
   FROM T_RESERVATION
   GROUP BY employee_id) reserv ON reserv.employee_id = emp.employee_id
START WITH emp.manager_id = 1 CONNECT BY
PRIOR emp.employee_id = emp.manager_id;
