SELECT Employee.NbrEmp "Nbr_employes", Reservation.NbrRes "Nbr_reserv", Customer.PrctAbon "Pourc_abos", Ticket.NbrTic "Nbr_tick", Trains.NbrTrain "Nbr_train", Stations.NbrSta "Nbr_station", Customer.NbrAch "Nbr_acheteurs"
FROM ( SELECT COUNT( DISTINCT emp.employee_id) NbrEmp
FROM t_employee emp ) Employee,
( SELECT COUNT( DISTINCT cus.customer_id) NbrAch, ROUND((COUNT(cus.pass_id) / COUNT( DISTINCT cus.customer_id) * 100),2) || '%' PrctAbon
FROM t_customer cus ) Customer,
( SELECT COUNT( DISTINCT res.reservation_id) NbrRes
FROM t_reservation res ) Reservation,
( SELECT COUNT( DISTINCT tic.ticket_id ) NbrTic
FROM t_ticket tic ) Ticket,
( SELECT COUNT( DISTINCT tr.train_id ) NbrTrain
FROM t_train tr ) Trains,
( SELECT COUNT( DISTINCT st.station_id ) NbrSta
FROM t_station st ) Stations;