SELECT COUNT(*) "Nombres Réservations Séniors"
FROM T_PASS pass
JOIN T_CUSTOMER cust
ON pass.pass_id = cust.pass_id
JOIN T_RESERVATION reserv 
ON cust.customer_id = reserv.buyer_id
JOIN T_TICKET ticket 
ON reserv.reservation_id = ticket.reservation_id
JOIN T_WAGON_TRAIN wagon 
ON ticket.wag_tr_id = wagon.wag_tr_id
JOIN T_TRAIN train 
ON wagon.train_id = train.train_id
WHERE pass.pass_name = 'Senior'
  AND train.departure_time BETWEEN '01/01/2018' AND LAST_DAY('01/01/2018')
  AND reserv.price IS NOT NULL
  AND reserv.creation_date BETWEEN cust.pass_date AND ADD_MONTHS(cust.pass_date, 12)
/
