SELECT train.train_id || ' ' || depart.city ||' - '|| arrive.city "Nom Train",
       SUM(wag.nb_seat) - COUNT(ticket.ticket_id) "Nombre de places libres"
FROM T_TRAIN train
	JOIN T_WAGON_TRAIN wagont 
	ON train.train_id = wagont.train_id
	JOIN T_WAGON wag 
	ON wag.wagon_id = wagont.wagon_id
	JOIN T_STATION depart 
	ON train.departure_station_id = depart.station_id
	JOIN T_STATION arrive 
	ON train.arrival_station_id = arrive.station_id
	JOIN T_TICKET ticket 
	ON ticket.wag_tr_id = wagont.wag_tr_id
	JOIN T_RESERVATION reserv 
	ON ticket.reservation_id = reserv.reservation_id
WHERE train.distance > 300
  AND train.departure_time LIKE TO_DATE('22/01/2018', 'DD/MM/YYYY')
GROUP BY train.train_id,
         depart.city,
         arrive.city
HAVING COUNT(ticket.ticket_id) > 0
ORDER BY 1;
