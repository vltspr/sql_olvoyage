SELECT r.reservation_id "Reservation", r.creation_date "Date",
       CONCAT(UPPER(e.last_name), CONCAT(' ', e.first_name)) "Employee", 
       CONCAT(UPPER(c.last_name), CONCAT(' ', c.first_name)) "Acheteur" 
FROM T_RESERVATION r 
JOIN T_EMPLOYEE e 
ON r.employee_id = e.employee_id 
JOIN T_CUSTOMER c 
ON c.customer_id = r.buyer_id 
WHERE reservation_id = 1;