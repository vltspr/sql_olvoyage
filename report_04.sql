SELECT CONCAT(UPPER(c.last_name), CONCAT(' ', c.first_name)) "NOM Pr�nom",
	    CASE  
			WHEN (Months_between('15/05/2018', c.pass_date)>12) THEN 'Perim� !'
			WHEN (Months_between('15/05/2018', c.pass_date) is NULL) THEN 'Aucun'
	        ELSE p.pass_name
        END "Abonnement"
FROM T_CUSTOMER c
JOIN T_PASS p
ON p.pass_id = c.pass_id
ORDER BY "NOM Pr�nom"
/
