SELECT pass.pass_name "Abonnement"
FROM T_PASS pass
JOIN T_CUSTOMER cust
ON cust.pass_id = pass.pass_id
GROUP BY pass.pass_name
ORDER BY COUNT(*) DESC
/
