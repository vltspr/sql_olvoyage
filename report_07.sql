SELECT *
FROM  (SELECT stDep.city ||' � '|| stArr.city "top 5 des trains"
FROM t_train tr
JOIN T_station stDep
ON tr.departure_station_id = stDep.station_id    
JOIN T_station stArr
ON tr.arrival_station_id = stArr.station_id
JOIN T_wagon_train twt
ON tr.Train_id = twt.train_id
JOIN T_ticket tic
ON twt.wag_tr_id = tic.wag_tr_id
GROUP BY stdep.city ||' � ' || stArr.city, train_id
ORDER BY COUNT(tic.TICKET_ID) DESC )
WHERE ROWNUM <=5;