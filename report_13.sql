SELECT ticket.ticket_id "Ticket ID",
       UPPER(cust.last_name) || ' ' || cust.first_name "Client",
       depart.city || TO_CHAR(train.departure_time, ' (DD/MM/YY HH24:MI) ') || '- ' || arrive.city || TO_CHAR(train.arrival_time, ' (DD/MM/YY HH24:MI) ') "Trajet"
FROM T_CUSTOMER cust
	JOIN T_RESERVATION reserv 
	ON cust.customer_id = reserv.buyer_id
	JOIN T_TICKET ticket 
	ON reserv.reservation_id = ticket.reservation_id
	JOIN T_WAGON_TRAIN wagont 
	ON ticket.wag_tr_id = wagont.wag_tr_id
	JOIN T_WAGON wag 
	ON wagont.wagon_id = wag.wagon_id
	JOIN T_TRAIN train 
	ON wagont.train_id = train.train_id
	JOIN T_STATION depart 
	ON train.departure_station_id = depart.station_id
	JOIN T_STATION arrive 
	ON train.arrival_station_id = arrive.station_id
WHERE wag.class_type = 1
  AND MONTHS_BETWEEN(cust.birth_date, reserv.creation_date) < 25*12
  AND train.departure_time BETWEEN '15/01/2018' AND '25/01/2018'
  AND (train.departure_time - reserv.creation_date) BETWEEN 0 AND 20
/
